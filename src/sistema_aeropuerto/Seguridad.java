package sistema_aeropuerto;

/**
 *
 * Esta clase tiene metodos utilitarios para encriptar y desencriptar texto. Es
 * una implementacion de lo que se conoce como "Cifrado César".
 */
public class Seguridad {

    // El valor de la compensacion debe mantenerse constante para poder
    // desencriptar. Puede ser cualquier numero (en una situacion real se
    // cambiaria el valor de compensacion cada cierto tiempo e.g. cada semana
    // o si se sospecha que ha habido una brecha de seguridad) pero se deja 
    // en 1 por defecto en este caso
    public static final int COMPENSACION_DE_CIFRADO = 1;

    static char[] caracteres = {
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
        'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
        'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
        'y', 'z', '0', '1', '2', '3', '4', '5',
        '6', '7', '8', '9', 'A', 'B', 'C', 'D',
        'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
        'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
        'U', 'V', 'W', 'X', 'Y', 'Z', '!', '@',
        '#', '$', '%', '^', '&', '(', ')', '+',
        '-', '*', '/', '[', ']', '{', '}', '=',
        '<', '>', '?', '_', '"', '.', ',', ' '
    };

    /**
     * Encripta texto segun el Cifrado César
     *
     * @param pwd la contrasena a encriptar
     * @param compensacion el valor de compensacion
     * @return texto encriptado
     */
    static String encriptar(String pwd) {
        pwd = pwd.trim();
        char[] plain = pwd.toCharArray();

        for (int i = 0; i < plain.length; i++) {
            for (int j = 0; j < caracteres.length; j++) {
                if (j <= caracteres.length - COMPENSACION_DE_CIFRADO) {
                    if (plain[i] == caracteres[j]) {
                        plain[i] = caracteres[j + COMPENSACION_DE_CIFRADO];
                        break;
                    }
                } else if (plain[i] == caracteres[j]) {
                    plain[i] = caracteres[j - (caracteres.length - COMPENSACION_DE_CIFRADO + 1)];
                }
            }
        }
        return String.valueOf(plain);
    }

    /**
     * Desencripta texto segun el Cifrado César si se conoce el valor de
     * compensacion original
     *
     * @param cif el texto cifrado
     * @param compensacion la compensacion tiene que ser igual que cuando se
     * encripto el text para que se pueda desencriptar
     * @return el texto original
     */
    static String desencriptar(String cif) {
        char[] cifro = cif.toCharArray();
        for (int i = 0; i < cifro.length; i++) {
            for (int j = 0; j < caracteres.length; j++) {
                if (j >= COMPENSACION_DE_CIFRADO && cifro[i] == caracteres[j]) {
                    cifro[i] = caracteres[j - COMPENSACION_DE_CIFRADO];
                    break;
                }
                if (cifro[i] == caracteres[j] && j < COMPENSACION_DE_CIFRADO) {
                    cifro[i] = caracteres[(caracteres.length - COMPENSACION_DE_CIFRADO + 1) + j];
                    break;
                }
            }
        }
        return String.valueOf(cifro);
    }
}
