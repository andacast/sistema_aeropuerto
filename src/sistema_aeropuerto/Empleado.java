/*
 * Clase padre la cual tiene todos los atributos y metodos comunes entre administrador , planificador y cajero
 */
package sistema_aeropuerto;

public abstract class Empleado implements Rutador_de_archivos {
    private String identificador,correo,departamento,usuario,clave,rol;
    Empleado(String identificador ,String correo,String departamento, String usuario, String clave, String rol){
        // se debe trabajar en la validacion de los datos por ejemplo el correo 
        this.correo = correo;
        this.departamento = departamento;
        this.identificador = identificador;
        this.usuario = usuario;
        this.clave = clave;
        this.rol = rol;
    }
    
    @Override
    public String toString() {
    return "Empleado :" + "identificador=" + this.getIdentificador() + ", correo=" + this.getCorreo() + 
            ", departamento=" + this.getDepartamento() + " usuario: " + this.getUsuario() +  '}';
    }

    public String getIdentificador() {
        return identificador;
    }

    public String getCorreo() {
        return correo;
    }

    public String getDepartamento() {
        return departamento;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getClave() {
        return clave;
    }

    public String getRol() {
        return rol;
    }

    
    
    
}
