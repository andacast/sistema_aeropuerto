/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema_aeropuerto;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que representa una reserva
 * @author acast
 */
public class Reserva {
    String codigo_de_empleado,numero_de_vuelo,codigo;
    LocalDate fecha_de_reserva;
    ArrayList<Boleto> lista_boletos;
    Vuelo vuelo;
    public Reserva(String codigo_de_empleado, String numero_de_vuelo,int numero_de_boletos,Vuelo v) {
        Scanner sc = new Scanner(System.in);
        System.out.println("ingrese codigo de la reserva");
        codigo = sc.nextLine();
        this.codigo_de_empleado = codigo_de_empleado;
        this.vuelo = v;
        this.numero_de_vuelo = numero_de_vuelo;
        this.lista_boletos = new ArrayList(); 
        for(int i=0;numero_de_boletos>i;i++ ){
            String asiento,nombre,cedula,apellido;
            LocalDate fecha_de_nacimiento;
 
                
                System.out.println("ingrese nombre pasajero");
                nombre = sc.nextLine();
                
                System.out.println("ingrese apellido del pasajero");
                apellido = sc.nextLine();
                
                System.out.println("ingrese cedula");
                cedula = sc.nextLine();
                
                System.out.println("ingrese asiento");
                asiento = sc.nextLine();
                boolean intento = true;
               fecha_de_nacimiento = null;
                while(intento){
                try{
                    System.out.println("Ingrese la fecha de nacimiento en formato dd/MM/yy :");
            String fecha_arribo1= sc.nextLine();
            DateTimeFormatter formatter3=DateTimeFormatter.ofPattern("dd/MM/yy");
            fecha_de_nacimiento = LocalDate.parse(fecha_arribo1,formatter3);
                intento=false;
                }catch(Exception e){
                    intento=true;
                    System.out.println("error al ingresar la fecha");
                }
                }
      
            Pasajero p = new Pasajero( nombre , apellido, cedula, fecha_de_nacimiento);
            lista_boletos.add(new Boleto(p,asiento,numero_de_vuelo)); 
        }
        //validar precio a pagar 
        double valor=0 ;
        for(Boleto b: lista_boletos){
            
            if(b.pasajero.fecha_de_nacimiento.isAfter( LocalDate.of(2017,01,01))){
                valor+=150;
            }else{
                valor+=300;
            }
            
        }
        valor =valor +( valor*0.12);
        try {
            guardar_ventas("ventas.txt",numero_de_boletos,valor);
        } catch (IOException ex) {
            Logger.getLogger(Reserva.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //codigoReserva,numeroVuelo,cantidadPasajeros,areopuertoOrigen, aeropuertoDestino,valor 
        public void guardar_ventas(String archivo,int numero_de_boletos,double valor) throws IOException{
        String cadena= codigo+","+numero_de_vuelo+","+ numero_de_boletos+","+"ecuador"+","+vuelo.lugar_de_arribo+","+valor ;
        FileWriter fw = new FileWriter(archivo,true);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(cadena+"\n");
        bw.close();
     }
}
