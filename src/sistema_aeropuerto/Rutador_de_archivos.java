/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema_aeropuerto;

/**
 * Intefaz utilitaria para compartir las direcciones de los archivos
 * @author acast
 */
public interface Rutador_de_archivos {
   public String RUTA_ARCHIVO_VUELOS = "vuelos.txt";
   public String RUTA_ARCHIVO_DISPONIBLES = "disponibles.txt";
   public String RUTA_ARCHIVO_VENTAS = "ventas.txt";
   public String RUTA_ARCHIVO_USUARIOS ="usuarios.txt";
   public String RUTA_ARCHIVO_AEROLINEAS = "aerolineas.txt";
   public String RUTA_ARCHIVO_AVIONES="aviones.txt";
}
