/*
 * clase que contiene el metodo main y por el momento sirve para validar el codigo que se implemente
 */
package sistema_aeropuerto;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Entrada del sistema
 * @author zen
 */
public class Main {

    public static void main(String[] args) {
        

        Sistema_Aeropuerto system = new Sistema_Aeropuerto();
        try {
            // Carga todos los valores que requiere el programa para funcionar
            // antes de iniciar
            system.actualizarDatos();
            // para poder debugear
            system.imprimirEmpleadosAConsola();
            system.imprimirAerolineasAConsola();
            // el inicio del programa
           
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    
}