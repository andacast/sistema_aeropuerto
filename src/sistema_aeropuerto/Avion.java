/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema_aeropuerto;

import java.util.HashMap;

/**
 * Clase que representa el objeto avion
 * @author acast
 */
public class Avion {
    int numero_de_serie;
    double distancia_maxima;
    String fabricante;
    String modelo;
    final HashMap<String,Integer> asientosHashMap;

    public Avion(int numero_de_serie, double distancia_maxima, String fabricante, String modelo, HashMap<String, Integer> asientos) {
        this.numero_de_serie = numero_de_serie;
        this.distancia_maxima = distancia_maxima;
        this.fabricante = fabricante;
        this.modelo = modelo;
        this.asientosHashMap = asientos;
    }

    public int getNumero_de_serie() {
        return numero_de_serie;
    }

    public double getDistancia_maxima() {
        return distancia_maxima;
    }

    public String getFabricante() {
        return fabricante;
    }

    public String getModelo() {
        return modelo;
    }

    public HashMap<String, Integer> getAsientosHashMap() {
        return asientosHashMap;
    }
    
    
    
}
