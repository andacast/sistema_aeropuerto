package sistema_aeropuerto;

// andres castro 
import Ventanas.VentanaCompra;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase maneja todo los relacionado al cajero y la venta de boletos
 * @author zen
 */
public class Cajero extends Empleado {

    Aerolinea aerolinea;
    Scanner sc;
    boolean menu_op;
    int valor_de_sc, boletos;
    private ArrayList<String> destinos;
    ArrayList<String> categorias;
    ArrayList<Vuelo> vuelos_disponibles;
    final static String departamento = "comercial";
    private Vuelo vuelo_seleccionado;
    private String destino, categoria;
    // constructor del cajero 
    public Cajero(String identificador, String correo, String departamento, String usuario, String clave, Aerolinea aerolinea, String rol) {
        super(identificador, correo, departamento, usuario, clave, rol);
        this.aerolinea = aerolinea;
    }

    //MOSTRAR VUELO CREA LISTAS DE LOS POSIBLES DESTINOS Y LAS CLASES QUE DISPONEN LOS VUELOS 
    public void mostrar_vuelo() {
        //se usan metodos para extraer los destinos 
        destinos = new ArrayList();
        categorias = new ArrayList();
        vuelos_disponibles = new ArrayList();
        aerolinea.cargar_vuelos();
       preguntar_destino();
    }
    //metodo que muestra los posibles destinos registrados en la aerolinea y pregunta por uno
    private void preguntar_destino() {
        for (Vuelo v : aerolinea.vuelos) {
            if (!destinos.contains(v.lugar_de_arribo)) {
                destinos.add(v.lugar_de_arribo);
            }
        }
    }

    //metodo que muestra las categorias de asientos de los aviones que tienen un destino preguntado 
    public void preguntar_categoria(String destino) {
        for (Vuelo v : aerolinea.vuelos) {
            if (destino.startsWith(v.lugar_de_arribo)) {
                for (String i : v.asientos_disponibles.keySet()) {
                    if (!categorias.contains(i)) {
                        categorias.add(i);
                    }
                }
            }
        }
        
    }

    public String[] mostrar_vuelos_filtrados(String categoria, String destino) { 
            ArrayList<String> str = new ArrayList();
            // se muestran los posibles VUELOS deacuerdo a la categoria y destino seleccionados
            for (Vuelo v : aerolinea.vuelos) {
                if (destino.startsWith(v.lugar_de_arribo)) {
                    str.add("VUELO:" + v.codigo_de_vuelo + ":: el numero disponible: " + v.asientos_disponibles.get(categoria) + " ");
                    vuelos_disponibles.add(v);
                    
                }
            }
            return GetStringArray(str);
    }
    
    public Vuelo buscar_vuelo_seleccionado(String vuelo_selecionado,String destino){
        String[] lista  = vuelo_selecionado.split(":");
        System.out.println(lista[1]);
        for (Vuelo v : aerolinea.vuelos) {
                if (destino.equalsIgnoreCase(v.lugar_de_arribo) && lista[1].equalsIgnoreCase(v.codigo_de_vuelo)) {
                    return v;
                }
            }
        return null;
    }
    public void ventan_compra(Cajero c,Vuelo vuelo_selecionado){
          
        java.awt.EventQueue.invokeLater(new Runnable() {
                        public void run() {
                            System.out.println("hola");
                            new VentanaCompra(c,vuelo_selecionado).setVisible(true);
                        }
                    });
    }
    /**
     * Modifica los archivos disponibles
     * @param v es el vuelo
     * @param val
     * @param categoria
     * @throws FileNotFoundException
     * @throws IOException cuando no encuentra el archivo
     */
    private void cambiar_archivos(Vuelo v, int val, String categoria) throws FileNotFoundException, IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(RUTA_ARCHIVO_DISPONIBLES));) {
            String line;
            String nueva_linea;
            ArrayList<String> lineas = new ArrayList();
            line = reader.readLine();
            while (line != null) {
                ArrayList<String> array_de_linea = new ArrayList();
                for (String s : line.split(",")) {
                    array_de_linea.add(s);
                }

                if (array_de_linea.get(0).contains(v.codigo_de_vuelo)) {
                    //se cambia el array_de_linea y se lo comvierte a un string
                    if (array_de_linea.get(1).contains(categoria)) {
                        Integer resta = parseInt(array_de_linea.get(2)) - val;
                        array_de_linea.add(2, resta.toString());
                        array_de_linea.remove(3);
                    } else {
                        Integer resta = parseInt(array_de_linea.get(4)) - val;
                        array_de_linea.add(4, resta.toString());
                        array_de_linea.remove(5);
                    }
                    nueva_linea = "";
                    for (String concatenar : array_de_linea) {
                        nueva_linea += concatenar + ",";
                    }
                    lineas.add(nueva_linea);
                } else {
                    lineas.add(line);
                }
                line = reader.readLine();
            }
            reader.close();
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(RUTA_ARCHIVO_DISPONIBLES));) {
                for (String l : lineas) {
                    writer.append(l + "\n");
                }
                writer.close();
            }
        }

    }

    public String[] getDestinos() {
       String[] str = GetStringArray(destinos); 
        return str;
    }
    public String[] getCategorias(){
        String[] str = GetStringArray(categorias);
        return str;
    }
    public static String[] GetStringArray(ArrayList<String> arr) 
    { 
  
        // declaration and initialise String Array 
        String str[] = new String[arr.size()]; 
  
        // ArrayList to Array Conversion 
        for (int j = 0; j < arr.size(); j++) { 
  
            // Assign each value to String array 
            str[j] = arr.get(j); 
        } 
  
        return str; 
    } 
    
    @Override
    public String toString() {
        return "Cajero :" + "identificador=" + this.getIdentificador() + ", correo=" + this.getCorreo() + ", departamento=" + this.getDepartamento() + '}';
    }
}
