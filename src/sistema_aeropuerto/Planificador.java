package sistema_aeropuerto;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.FileWriter;
import static java.lang.Integer.parseInt;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase que representa a un planificador
 * @author zen
 */
public class Planificador extends Empleado {

    Scanner scanner = new Scanner(System.in);
    Aerolinea aerolinea;

    public Planificador(String identificador, String correo, String departamento, String usuario, String clave, String rol, Aerolinea aerolinea) {
        super(identificador, correo, departamento, usuario, clave, rol);
        //se debe crear un metodo que llene los datos de la aerolinea
        this.aerolinea = aerolinea;
    }
    
    /**
     * Pide los datos necesarios para crear un avion por consola, y luego los guarda
     * en un archivo de texto.
     */
    public void ingresarNuevoAvionEnArchivoTxt() {
        
        Avion avion = generarObjetoAvionDesdeConsola();
        System.out.println("\nSe va a intentar guardar los datos del avion en "
                + "la base de datos...");
        
        guardarDatosDeAvionEnTxt(avion);
        System.out.println("Guardado exitoso!");
    }
    
    /**
     * Guarda todos los datos de un avion a un archivo de texto, en el siguiente orden:
     * <p>
     * numSerie_del_avion,aerolinea,fabricante,modelo,asientos_negocios,asientos_economicos,maxkm
     * 
     * @param avion es el avion a procesar
     */
    private void guardarDatosDeAvionEnTxt(Avion avion){
        // Los datos del avion a guardar
        String datosAvion = String.valueOf(avion.getNumero_de_serie()) + "," + this.aerolinea.getNombre() + "," + 
                avion.getFabricante() + "," + avion.getModelo() + "," + 
                avion.getAsientosHashMap().get("negocios") + "," + avion.getAsientosHashMap().get("economico") + "," + 
                avion.getDistancia_maxima() + "\n";
        
        // Guardando usando try-with-resources
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(RUTA_ARCHIVO_AVIONES,true))){
            bw.write(datosAvion);
        } catch (IOException e){
            System.out.println("Error al buscar el archivo de aviones:\n" + e);
        }
    }

    public void guardar_datos_aviones(File archivo, String serie, String fabricante, 
            String modelo, String economicos, String negocios, String distancia) throws IOException {
        
        String cadena = serie + "," + fabricante + "," + modelo + "," + economicos + "," + negocios + "," + distancia;
        FileWriter fw = new FileWriter(archivo, true);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("\n");
        bw.write(cadena);
        bw.close();
    }

    /**
     * Piden por consola los datos para crear un objeto avion.
     * 
     * @return el objeto avion segun los datos dados
     */
    private Avion generarObjetoAvionDesdeConsola(){
        System.out.println("Se va a crear un nuevo avion para asignar a la "
                + "aerolinea " + this.aerolinea.getNombre() + ". Ingrese los datos "
                + "necesarios para la creacion...\n");

        System.out.println("Ingrese el numero de serie:");
        String numero_de_serie = scanner.nextLine();

        System.out.println("Ingrese el fabricante:");
        String fabricante = scanner.nextLine();

        System.out.println("Ingrese el modelo:");
        String modelo = scanner.nextLine();

        System.out.println("Ingrese la capacidad de asientos economicos:");
        String capacidad_asientos_economicos = scanner.nextLine();

        System.out.println("Ingrese la capacidad de asientos de negocios:");
        String capacidad_asientos_negocios = scanner.nextLine();

        System.out.println("Ingrese el distancia maxima del avion:");
        String distancia_max = scanner.nextLine();

        HashMap<String, Integer> asientos_disponibles = new HashMap();
        asientos_disponibles.put("negocios", Integer.valueOf(capacidad_asientos_negocios));
        asientos_disponibles.put("economico", Integer.valueOf(capacidad_asientos_economicos));

        return new Avion(Integer.valueOf(numero_de_serie),Double.valueOf(distancia_max),fabricante,modelo,asientos_disponibles);
    }

    /**
     * Pregunta al usuario si desea repetir la accion que acaba de hacer.
     * 
     * @return verdadero solo si el usuario escribe 's' o 'S'
     */
    public static boolean usuarioDeseaRepetirAccion(){
        System.out.println("Desea repetir esta accion? s/n");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        return input.equalsIgnoreCase("s");
    }

    /**
     * Metodo principal para la planificacion de nuevos vuelos. Se asume que este metodo se llama desde
     * Sistema_Aeropuerto. Pide al usuario el ingreso de los datos necesarios para la creacion de un vuelo,
     * realizando las validaciones debidas; con ellas, crea un objeto vuelo y luego escribe sus datos en
     * un archivo de texto.
     * <p>
     * Si funciona el ingreso, imprime "creado" por pantalla. De lo contrario, si existe algun error se
     * volvera a llamar a si mismo recursivamente hasta que se logre crear un vuelo con exito.
     * 
     */
    public void planificar_vuelo() {
        aerolinea.cargar_vuelos();
        aerolinea.cargar_aviones();
        //verificar existencia de el avion en la aerolinea
        int cont = 0;
        boolean bandera = true;
        String avion = "";
        while (bandera) {
            System.out.println("ingrese el codigo del avion");
            avion = scanner.nextLine();
            for (Avion a : aerolinea.lista_de_aviones) {
                if (a.numero_de_serie == parseInt(avion)) {
                    cont++;
                }
            }
            if (cont > 0) {
                bandera = false;
            }
        }
        try {
            System.out.println("Ingrese el codigo de vuelo:");
            String codigo_vuelo = scanner.nextLine();

            System.out.println("Ingrese el codigo_IATA1_arribo:");
            String codigo_IATA1_arribo = scanner.nextLine();

            System.out.println("Ingrese el codigo_IATA1_arribo:");
            String codigo_IATA1_salida = scanner.nextLine();

            String fecha_embarque1;
            do {
                System.out.println("Ingrese la fecha de embarque en formato dd-MM-yyyy :");
                fecha_embarque1 = scanner.nextLine();
            } while (!esValidaFecha(fecha_embarque1));
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate fecha_de_embarque = LocalDate.parse(fecha_embarque1, formatter);
            
            String fecha_salida1;
            do {
                System.out.println("Ingrese la fecha de salida en formato dd-MM-yyyy :");
                fecha_salida1 = scanner.nextLine();
            } while (!esValidaFecha(fecha_salida1));
            LocalDate fecha_de_salida = LocalDate.parse(fecha_salida1, formatter);

            String fecha_arribo1;
            do {
                System.out.println("Ingrese la fecha de arribo en formato dd-MM-yyyy :");
                fecha_arribo1 = scanner.nextLine();
            } while (!esValidaFecha(fecha_arribo1));
            LocalDate fecha_de_arribo = LocalDate.parse(fecha_arribo1, formatter);

            String hora_embarque1;
            do {
                System.out.println("Ingrese la  hora de embarque en formato hh:mm:ss :");
                hora_embarque1 = scanner.nextLine();
            } while (!esValidaHora(hora_embarque1));
            formatter = DateTimeFormatter.ISO_LOCAL_TIME;
            LocalTime hora_de_embarque = LocalTime.parse(hora_embarque1, formatter);

            String hora_salida1;
            do {
                System.out.println("Ingrese la  hora de salida en formato hh:mm:ss :");
                hora_salida1 = scanner.nextLine();
            } while (!esValidaHora(hora_salida1));
            LocalTime hora_de_salida = LocalTime.parse(hora_salida1, formatter);

            String hora_arribo1;
            do {
                System.out.println("Ingrese la  hora de arribo en formato hh:mm:ss :");
                hora_arribo1 = scanner.nextLine();
            } while (!esValidaHora(hora_arribo1));
            LocalTime hora_de_arribo = LocalTime.parse(hora_arribo1, formatter);

            System.out.println("Ingrese la puerta de arribo:");
            String puerta_de_arribo = scanner.nextLine();

            System.out.println("Ingrese la puerta salida:");
            String puerta_de_salida = scanner.nextLine();

            System.out.println("Ingrese los asientos por categoria  categorias del avion:negocios");
            String asientos_negocios = scanner.nextLine();
            
            System.out.println("Ingrese los asientos por categoria  categorias del avion:economico");
            String asientos_economico = scanner.nextLine();

            System.out.println("Ingrese el lugar de arribo:");
            String lugar_de_arribo = scanner.nextLine();
            
            //  LocalDate fecha_de_salida, LocalDate fecha_de_arribo, LocalTime hora_de_embarque, LocalTime hora_de_arribo, LocalTime hora_de_salida, String puerta_de_salida, String puerta_de_arribo, String categorias, String lugar_de_arribo)
            Vuelo v = new Vuelo(codigo_vuelo,codigo_IATA1_arribo,codigo_IATA1_salida,fecha_de_embarque,fecha_de_salida,fecha_de_arribo,hora_de_embarque,hora_de_salida,hora_de_arribo,puerta_de_arribo,puerta_de_salida,asientos_negocios,asientos_economico,lugar_de_arribo);
            escribirVueloEnArchivo(v);
        } catch (Exception e) {
            System.out.println(e);
            planificar_vuelo();
        }
        System.out.println("creado");
    }

    /**
     * Escribe los datos de un objeto Vuelo en un archivo de texto.
     * 
     * @param v es el objeto vuelo a guardar
     * @throws IOException si el archivo de vuelos se corrompe
     */
    public void escribirVueloEnArchivo(Vuelo v) throws IOException {

        String textoAEscribir = v.codigo_de_vuelo + "," + v.codigo_IATA1_arribo + "," + v.codigo_IATA1_salida + "," + v.fecha_de_embarque.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + "," + v.fecha_de_salida.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + "," + v.fecha_de_arribo.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + "," + v.hora_de_embarque + "," + v.hora_de_arribo + "," + v.hora_de_salida + "," + v.puerta_de_salida + "," + v.puerta_de_arribo + "," + v.asientos_disponibles.get("negocios") + "," + v.asientos_disponibles.get("economico") + "," + v.lugar_de_arribo + "," + this.aerolinea.getNombre();
        FileWriter fw = new FileWriter(RUTA_ARCHIVO_VUELOS, true);
        BufferedWriter bw = new BufferedWriter(fw);

        bw.write(textoAEscribir + "\n");
        bw.close();
    }

    /**
     * Verifica que la fecha sea valida en formato dd-MM-yyyy
     *
     * @param fecha es la fecha a verificar
     * @return verdadero si el formato es valido
     */
    private boolean esValidaFecha(String fecha) {
        String regex = "^(3[01]|[12][0-9]|0[1-9])-(1[0-2]|0[1-9])-[0-9]{4}$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(fecha);
        return matcher.matches();

    }

    /**
     * Verifica que la hora sea valida en formato hh:mm:ss
     *
     * @param hora es la hora a verificar
     * @return verdadero si el formato es valido
     */
    public static boolean esValidaHora(String hora) {
        String regex = "(([0-1]?[0-9])|(2[0-3])):[0-5][0-9]:[0-5][0-9]";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(hora);
        return matcher.matches();

    }



    public void guardar_datos_disponibles(String codigo, String categorias) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("disponibles.txt", true));) {
            String[] s = categorias.split(";");
            writer.newLine();
            writer.write(codigo + ",negocios," + s[0] + ",economico," + s[1]);

            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(Planificador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
