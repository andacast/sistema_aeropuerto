package sistema_aeropuerto;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Esta clase se encarga de varias funciones administrativas.
 *
 * @author Cristobal Salazar
 */
public class Administrador extends Empleado {

    private static enum DEPARTAMENTO {COMERCIAL, SISTEMAS}; 

    Administrador(String identificador, String correo, String departamento, String usuario, String clave, String rol) {
        super(identificador, correo, departamento, usuario, clave, rol);
    }

    @Override
    public String toString() {
        return "Admin :" + "identificador=" + this.getIdentificador() + ", correo=" + this.getCorreo()
                + ", departamento=" + this.getDepartamento() + '}';
    }

    /**
     * Valida que las contraseñas cumplan los requisitos: min 8 caracteres min 1
     * caracter en uppercase min 1 num
     *
     * @param pwd la contraseña a comprobar
     * @return true si es valida, falso de lo contrario
     */
    boolean esContraseñaValida(String pwd) {

        boolean min8Chars = pwd.length() >= 8;

        boolean min1Uppercase = false;
        for (char x : pwd.toCharArray()) {
            if (Character.isUpperCase(x)) {
                min1Uppercase = true;
            }
        }

        boolean min1Num = false;
        for (char x : pwd.toCharArray()) {
            if (Character.isDigit(x)) {
                min1Num = true;
            }
        }

        return min1Num && min1Uppercase && min8Chars;

    }

    /**
     * Verifica que la cedula sea valida segun los parametro establecidos:
     * <ul>
     * <li>
     * La cédula ecuatoriana está formada por los dos primeros dígitos que
     * corresponden a la provincia donde fue expedida, por lo cual, los dos
     * primeros dígitos no serán mayores a 24 ni menores a 0.
     * <li>
     * El tercer dígito es un número menor a 6 (0,1,2,3,4,5)
     * <li>
     * Los siguientes hasta el noveno dígito son un número consecutivo
     * <li>
     * El décimo es el dígito verificador
     * </ul>
     *
     * @param cedula la cedula a verificar
     * @return true si es valida, false de lo contrario
     */
     public boolean esCedulaValida(String cedula) {

        char[] cedulaArray = cedula.toCharArray();

        if (cedula.length() != 10) {
            return false;
        }

        // verifica que los dos primeros dígitos no serán mayores a 24 ni menores a 0
        int dosPrimerosDigitos = Integer.valueOf(String.valueOf(cedulaArray[0]) + String.valueOf(cedulaArray[1]));
        if (dosPrimerosDigitos > 24 || dosPrimerosDigitos < 0) {
            return false;
        }

        // verifica que la cedula solo tenga numeros
        for (char numero : cedula.toCharArray()) {
            if (!Character.isDigit(numero)) {
                return false;
            }
        }

        // verifica que el tercer digito sea menor a 6
        if (Character.getNumericValue(cedulaArray[2]) >= 6) {
            return false;
        }

        // comprueba digito verificador        
        return generaDigitoVerificador(cedulaArray) == Character.getNumericValue(cedulaArray[9]);
    }

    /**
     * Verifica que el rol ingresado sea uno de los 3 posibles.
     * <p>
     * Los tres posibles roles son:
     * <ul>
     * <li> Administrador: A
     * <li> Planificador: P
     * <li> Cajero: C
     * </ul>
     *
     * @param rol el rol a verificar
     * @return verdadero si el rol es valido, falso de lo contrario
     */
    boolean esRolValido(String rol) {
        rol = rol.toUpperCase();
        switch (rol) {
            case "A":
                return true;
            case "P":
                return true;
            case "C":
                return true;
            default:
                return false;
        }
    }

    /**
     * Genera un digito verificador. Este digito se comprueba con el ultimo
     * digito de la cedula, si son iguales, la cedula es valida. Los parametro
     * se pueden verificar en este
     * <a href="https://medium.com/@bryansuarez/c%C3%B3mo-validar-c%C3%A9dula-y-ruc-en-ecuador-b62c5666186f">link</a>.
     *
     * @param cedulaArray la cedula a comprobar convertida en un char[] de
     * numeros
     * @return el digito verificador
     */
    private int generaDigitoVerificador(char[] cedulaArray) {
        int resultado = 0;
        char[] digitosParaComprobar = new char[9];
        System.arraycopy(cedulaArray, 0, digitosParaComprobar, 0, 9);
        final int[] COEFICIENTES = {2, 1, 2, 1, 2, 1, 2, 1, 2};

        for (int i = 0; i < 9; i++) {
            int producto = Character.getNumericValue(digitosParaComprobar[i]) * COEFICIENTES[i];
            if (producto >= 10) {
                producto -= 9;
            }
            resultado += producto;
        }

        // Al resultado se le resta la decena superior
        int decenaSuperior = resultado;
        while (decenaSuperior % 10 != 0) {
            decenaSuperior += 1;
        }
        resultado = decenaSuperior - resultado;

        return resultado;
    }

    /**
     * Guarda la informacion de un usuario en un archivo .txt Asume que la clave
     * y la cedula ya han sido validadas
     *
     * @param cedula
     * @param nombre
     * @param apellido
     * @param email
     * @param usuario el nombre de usuario del usuario
     * @param contraseña lo que el usuario desea que sea su clave. Esta clave
     * sera encriptada antes de ser guardada en el .txt
     * @param rol
     * @param aerolinea
     * @param departamento
     * @throws IOException si se ha movido o borrado el archivo [usuarios.txt]
     */
    public void guardarUsuarioEnArchivo(String cedula, String nombre, String apellido,
            String email, String usuario, String contraseña, String rol,
            String aerolinea, String departamento) throws IOException {

        contraseña = Seguridad.encriptar(contraseña);
        String str = cedula + "," + nombre + "," + apellido + "," + email + "," + usuario + "," + contraseña + "," + rol
                + "," + aerolinea + "," + departamento + "\n";
        try (BufferedWriter writer = 
                new BufferedWriter(new FileWriter(RUTA_ARCHIVO_USUARIOS, true))) {
            writer.append(str);
        }
    }

    /**
     * Guarda la informacion de una aerolinea en un archivo .txt
     *
     * @param nombre El nombre de la aerolinea
     * @throws IOException si se ha movido o borrado el archivo [aerolineas.txt]
     */
    public void guardarAerolineaEnArchivo(String nombre) throws IOException {
        try (BufferedWriter writer = 
                new BufferedWriter(new FileWriter(RUTA_ARCHIVO_AEROLINEAS, true))) {
            writer.append(nombre + "\n");
        }
    }
    
    /**
     * Toma el rol de un empleado y asigna el departamento correspondiente
     * a ese rol.
     * 
     * @param rol es el rol del empleado
     * @return el departamento que le corresponde
     */
    public String asignaDepartamentoSegunRol(String rol){
        switch(rol){
            case "C":
                return DEPARTAMENTO.COMERCIAL.toString();
            case "P":
                return DEPARTAMENTO.COMERCIAL.toString();
            case "A":
                return DEPARTAMENTO.SISTEMAS.toString();
            default:
                return "ERROR de asignacion de rol";
        }
    }
}
