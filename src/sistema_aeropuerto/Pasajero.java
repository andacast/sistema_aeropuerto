/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema_aeropuerto;

import java.time.LocalDate;

/**
 * Clase que representa a un cajero
 * @author acast
 */
class Pasajero {
    String cedula,nombre,apellido;
    LocalDate fecha_de_nacimiento;
    Pasajero(String nombre ,String apellido,String cedula,LocalDate fecha_nacimiento){
       this.cedula = cedula ;
       this.nombre = nombre;
       this.apellido = apellido;
       this.fecha_de_nacimiento = fecha_nacimiento;
    }
}
