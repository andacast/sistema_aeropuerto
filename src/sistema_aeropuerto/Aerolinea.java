package sistema_aeropuerto;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que representa una aerolinea
 * @author zen
 */
public class Aerolinea implements Rutador_de_archivos{

    String nombre;
    ArrayList<Avion> lista_de_aviones;
    ArrayList<Vuelo> vuelos;
    HashMap<Integer, Avion> gates;

    Aerolinea(String nombre) {
        this.nombre = nombre;
        lista_de_aviones = new ArrayList();
        vuelos = new ArrayList();
    }

    public void agregar_avion(Avion avion) {
        lista_de_aviones.add(avion);
    }

    public void agregar_vuelo(Vuelo vuelo) {
        vuelos.add(vuelo);
    }

    public boolean verificar_vuelo(Vuelo vuelo) {
        return false;
    }

    /**
     * Revisa el archivo donde se han guardado textualmente los vuelos, y actualiza la informacion
     * del ArrayList<Vuelo> de esta clase.
     */
    public void cargar_vuelos() {
        vuelos.clear();
        try (BufferedReader reader = new BufferedReader(new FileReader(RUTA_ARCHIVO_VUELOS));) {
            reader.readLine();
            String lineaDeArchivo;
            lineaDeArchivo = reader.readLine();
            while (lineaDeArchivo != null) {
                ArrayList<String> array_de_linea = new ArrayList();
                for (String s : lineaDeArchivo.split(",")) {
                    array_de_linea.add(s);
                }
                if(array_de_linea.get(14).equalsIgnoreCase(nombre)){
                vuelos.add(new Vuelo(array_de_linea.get(0), array_de_linea.get(1), array_de_linea.get(2), parselocaldate(array_de_linea.get(3)), parselocaldate(array_de_linea.get(4)), parselocaldate(array_de_linea.get(5)), parselocaltime(array_de_linea.get(6)), parselocaltime(array_de_linea.get(7)), parselocaltime(array_de_linea.get(8)), array_de_linea.get(9), array_de_linea.get(10), array_de_linea.get(11), array_de_linea.get(12),array_de_linea.get(13)));
                }
                lineaDeArchivo = reader.readLine();
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cajero.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cajero.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(RUTA_ARCHIVO_DISPONIBLES));) {
            String line;
            line = reader.readLine();
            while (line != null) {
                ArrayList<String> array_de_linea = new ArrayList();
                for (String s : line.split(",")) {
                    array_de_linea.add(s);
                }
                for (Vuelo v : vuelos) {
                    if (v.codigo_de_vuelo.equalsIgnoreCase(array_de_linea.get(0))) {
                       
                        v.asientos_disponibles.replace("negocios", parseInt(array_de_linea.get(2)));
                        v.asientos_disponibles.replace("economico", parseInt(array_de_linea.get(4)));
                        
                    }
                }
                line = reader.readLine();
            }
        } catch (Exception e) {
            System.out.println("error al cargar archivos");
        }
    }
    
    /**
     * Revisa el archivo donde se han guardado textualmente los vuelos, y actualiza la informacion
     * del ArrayList<Avion> de esta clase.
     */
    public void cargar_aviones() {
        lista_de_aviones.clear();
        try (BufferedReader reader = new BufferedReader(new FileReader(RUTA_ARCHIVO_AVIONES));) {
            String line;
            line = reader.readLine();
            while (line != null) {
                ArrayList<String> array_de_linea = new ArrayList();
                for (String s : line.split(",")) {
                    array_de_linea.add(s);
                }
                //0codigo_del_avion,1aerolinea,2fabricante,3modelo,4asientos_negocios,5asientos_economicos,6maxkm
                //int numero_de_serie, double distancia_maxima, String fabricante, String modelo, HashMap<String, Integer> asientos
                if(array_de_linea.get(1).equalsIgnoreCase(this.nombre)){
                    HashMap<String,Integer> hm = new HashMap();
                hm.put("negocios", parseInt(array_de_linea.get(4)));
                hm.put("economico", parseInt(array_de_linea.get(5)));
                    lista_de_aviones.add(new Avion(parseInt(array_de_linea.get(0)),Double.valueOf(array_de_linea.get(6)),array_de_linea.get(2),array_de_linea.get(3),hm));
                }
                line = reader.readLine();
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cajero.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cajero.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * Convierte un String que representa una fecha en un objeto LocalDate. Se asume que
     * el String esta en formato dd/MM/yyyy.
     * 
     * @param s es el String a convertir
     * @return el String convertido a su correspondiente objeto LocalDate
     */
    private LocalDate parselocaldate(String s) {
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate fecha = LocalDate.parse(s, formatter1);
        return fecha;
    }
    
    /**
     * Convierte un String que representa una hora en un objeto LocalTime. Se asume que
     * el String esta en formato hh:mm:ss.
     * 
     * @param s es el String a convertir
     * @return el String convertido a su correspondiente objeto LocalTime
     */
    private LocalTime parselocaltime(String s) {
        DateTimeFormatter dtf2 = DateTimeFormatter.ISO_LOCAL_TIME;
        LocalTime hora = LocalTime.parse(s, dtf2);
        return hora;
    }

    public String getNombre() {
        return nombre;
    }
    
    

    @Override
    public String toString() {
        return "Objeto Aerolinea. Nombre: " + nombre;
    }
}
