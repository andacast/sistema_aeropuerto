package sistema_aeropuerto;

import Ventanas.VentanaCajero;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Clase principal que coordina las otras clases para dar funcionalidad al
 * programa.
 */
public class Sistema_Aeropuerto implements Rutador_de_archivos {

    private ArrayList<Empleado> empleados;
    private ArrayList<Aerolinea> aerolineas;
    private Scanner scanner = new Scanner(System.in);
    // El usuario actual usando la terminal
    private Empleado usuarioActual;
    private Administrador adminActual;
    private Cajero cajeroActual;
    private Planificador planificadorActual;
    //menu planificador
    File fileAviones;
    File fileVuelos;
    String confirmacion;
    boolean continuar = false;
    Vuelo vuelo;

    /**
     * El menu de ingreso al sistema. Valida tanto usuario como la clave del
     * usuario. Continuara en repeat hasta que se valide al menos un usuario
     * correcto.
     */
    public boolean menuLogin(String usuario, String clave) throws IOException {
        actualizarDatos();
        clave = Seguridad.encriptar(clave);
        if (validarLogin(usuario, clave)) {
            System.out.println("Bienvenido!\n");
            usuarioActual = actualizarUsuarioActual(usuario);

            switch (usuarioActual.getRol().toLowerCase()) {
                case "a":
                    menuAdmin();
                    return true;
                    
                case "c":
                    menuCajero();
                    return true;
                case "p":
                    menuPlanificador();
                    return true;

            }

        } else {
            
            System.out.println("Error de ingreso, reiniciando programa...\n");
            return false;
        }
        return false;
    }

    /**
     * El menu que permite al administrador ejercer sus funciones.
     *
     * @throws IOException si los archivos donde se guardan las info se alteran
     * inesperadamente el programa no puede guardar la informacion
     */
    void menuAdmin() throws IOException {
        // El usuario actual es un admin
        adminActual = (Administrador) usuarioActual;

        System.out.println("");
        System.out.println("*********************");
        System.out.println(" MENU ADMINISTRADOR");
        System.out.println("*********************");
        System.out.println("");

        boolean deseaCerraSesion = false;
        // El menu corre hasta que cierre la sesion
        do {

            System.out.println("");
            System.out.println("1. Crear usuario");
            System.out.println("2. Crear aerolinea");
            System.out.println("3. Cerrar la sesion actual");

            System.out.println("Escoga una opcion: ");
            String userInput = scanner.nextLine();

            if (!esUnUnicoDigito(userInput)) {
                do {
                    System.out.println("Opcion incorrecta, no es un unico digito!\n"
                            + "Intente de nuevo: ");
                    System.out.println("");
                    System.out.println("1. Crear usuario");
                    System.out.println("2. Crear aerolinea");
                    System.out.println("3. Cerrar la sesion actual");

                    System.out.println("Escoga una opcion: ");
                    userInput = scanner.nextLine();
                } while (!esUnUnicoDigito(userInput));
            }

            int seleccionMenuUsuario = Integer.valueOf(userInput);
            // El usuario no puede hacer nada mas que elegir una de las opciones que
            // se le presentan
            // Es posible hacer refactor de esta seccion pues un switch puede evaluar
            // Strings; no es necesario llevarlo a int
            switch (seleccionMenuUsuario) {
                case 1:
                    promptCrearUsuario(adminActual);
                    break;
                case 2:
                    promptCrearAerolinea(adminActual);
                    break;
                case 3:
                    deseaCerraSesion = true;
                    System.out.println("Cerrando sesion...");
                    break;
                default:
                    System.out.println("Error! Seleccion invalida, el numero input es incorrecto"
                            + "\nIntente de nuevo:");
            }
        } while (!deseaCerraSesion);

    }

    /**
     * Si el administrador selecciona la opcion crear Aerolinea, este metodo le
     * permitira hacerlo. Las Aerolineas creadas se guardaran en
     * [aerolineas.txt].
     *
     * @param admin el administrador que actualmente esta usando la consola
     * @throws IOException si los archivos donde se guardan las info se alteran
     * inesperadamente el programa no puede guardar la informacion
     */
    private void promptCrearAerolinea(Administrador admin) throws IOException {

        System.out.println("Creando una nueva Aerolinea... Ingrese el nombre de la"
                + " nueva aerolinea: ");
        String nombre = scanner.nextLine();
        admin.guardarAerolineaEnArchivo(nombre);
        System.out.println("Aerolinea guardada en archivo exitosamente\n");
    }

    /**
     * Si el administrador selecciona la opcion crear usuario, este metodo le
     * permitira hacerlo. Los usuarios creados se guardaran en [usuarios.txt].
     *
     * @param admin el administrador que actualmente esta usando la consola
     * @throws IOException si los archivos donde se guardan las info se alteran
     * inesperadamente el programa no puede guardar la informacion
     */
    private void promptCrearUsuario(Administrador admin) throws IOException {

        String cedula, nombre, apellido,
                email, usuario, contraseña, rol,
                aerolinea;

        // Esta seccion valida todas las entradas del usuario
        System.out.println("Creando usuario... ingrese los siguientes datos...");
        System.out.println("Ingrese una cedula valida:");
        do {
            cedula = scanner.nextLine();
        } while (!admin.esCedulaValida(cedula));
        System.out.println("Cedula es valida");
        System.out.println("Ingrese un nombre:");
        nombre = scanner.nextLine();
        System.out.println("Ingrese un apellido:");
        apellido = scanner.nextLine();
        System.out.println("Ingrese un email:");
        email = scanner.nextLine();
        System.out.println("Ingrese un nombre de usuario:");
        usuario = scanner.nextLine();
        System.out.println("Ingrese una clave valida, requerimientos minimos:\n"
                + "-min 8 caracteres\n"
                + "-min 1 caracter es mayuscula\n"
                + "-min 1 num");
        do {
            contraseña = scanner.nextLine();
        } while (!admin.esContraseñaValida(contraseña));
        System.out.println("Contraseña es valida");
        System.out.println("Ingrese el rol; solo puede ingresar 'A',"
                + " 'P', o 'C' ");
        do {
            rol = scanner.nextLine().toUpperCase();
        } while (!admin.esRolValido(rol));
        System.out.println("Rol es valido");

        do {
            System.out.println("Ingrese el nombre de la aerolinea, si no"
                    + " pertenece a una aerolinea puede dejar en blanco");
            aerolinea = scanner.nextLine();
        } while (!existeAerolineaPreviamente(aerolinea));

        System.out.println("\nTodos los datos han sido ingresados, creado"
                + " usuario...");

        String departamento = admin.asignaDepartamentoSegunRol(rol);

        admin.guardarUsuarioEnArchivo(cedula, nombre, apellido, email, usuario, contraseña, rol, aerolinea, departamento);
        System.out.println("Usuario guardado en archivo exitosamente\n");

    }

    /**
     * Presenta al usuario el menu del cajero
     */
    void menuCajero() {
        // El usuario actual es un admin
        cajeroActual = (Cajero) usuarioActual;
                   java.awt.EventQueue.invokeLater(new Runnable() {
                        public void run() {
                            new VentanaCajero(cajeroActual).setVisible(true);
                        }
                    });
            }
      
    

    /**
     * Muestra al usuario el menu del planificador
     *
     * @throws IOException cuando no encuentra el archivo
     */
    void menuPlanificador() throws IOException {
        // El usuario actual es un admin
        planificadorActual = (Planificador) usuarioActual;

        System.out.println("");
        System.out.println("*********************");
        System.out.println(" MENU PLANIFICADOR");
        System.out.println("*********************");
        System.out.println("");

        boolean deseaCerraSesion = false;
        do {

            System.out.println("");
            System.out.println("1.Planificar vuelo ");
            System.out.println("2.Ingresar avion");
            System.out.println("3.Salir");

            System.out.println("Escoga una opcion: ");
            String userInput = scanner.nextLine();

            if (!esUnUnicoDigito(userInput)) {
                do {
                    System.out.println("Opcion incorrecta, no es un unico digito!\n"
                            + "Intente de nuevo: ");
                    System.out.println("");
                    System.out.println("1.Planificar vuelo");
                    System.out.println("2.Ingresar avion");
                    System.out.println("3.Salir");

                    System.out.println("Escoga una opcion: ");
                    userInput = scanner.nextLine();
                } while (!esUnUnicoDigito(userInput));
            }

            int seleccionMenuUsuario = Integer.valueOf(userInput);
            switch (seleccionMenuUsuario) {
                case 1:
                    do {
                        planificadorActual.planificar_vuelo();
                    } while (Planificador.usuarioDeseaRepetirAccion());

                    break;
                case 2:
                    do {
                        planificadorActual.ingresarNuevoAvionEnArchivoTxt();
                    } while (Planificador.usuarioDeseaRepetirAccion());
                    break;
                case 3:
                    deseaCerraSesion = true;
                    System.out.println("Cerrando sesion...");
                    break;
                default:
                    System.out.println("Error! Seleccion invalida, el numero input es incorrecto"
                            + "\nIntente de nuevo:");
            }
        } while (!deseaCerraSesion);
    }

    /**
     * Una vez que hay login con exito, se usa este metodo que el programa
     * conozca que objeto
     *
     * @param usuario
     * @return
     */
    private Empleado actualizarUsuarioActual(String usuario) {
        for (Empleado empleado : empleados) {
            if (empleado.getUsuario().equals(usuario)) {
                System.out.println(empleado);
                return empleado;
            }

        }

        return null;
    }

    /**
     * Verifica que el String ingresado sea unico (lenght == 1) y que represente
     * un digito
     *
     * @param string
     * @return verdadero o false dependiendo si cumple
     */
    private boolean esUnUnicoDigito(String string) {
        if (string.length() != 1) {
            return false;
        }
        if (!Character.isDigit(string.toCharArray()[0])) {
            return false;
        }

        return true;
    }

    /**
     * Actualiza la lista que contiene a todos los objetos relevantes al
     * sistema, leyendo un archivo de texto csv con la informacion de ellos.
     *
     * @throws IOException si los archivos de texto son borrados o se les cambia
     * la dirreccion
     */
    void actualizarDatos() throws IOException {
        empleados = cargarEmpleados();
        aerolineas = cargarAerolineas();
    }

    private ArrayList<Aerolinea> cargarAerolineas() throws IOException {
        ArrayList<Aerolinea> aerolineas = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(RUTA_ARCHIVO_AEROLINEAS))) {
            // Salta primera linea porque no contiene info relevante
            br.readLine();

            String linea;
            while ((linea = br.readLine()) != null) {
                aerolineas.add(procesaAerolineaDesdeStringCSV(linea));
            }
        }

        return aerolineas;
    }

    /**
     * De una linea de texto obtenida en un archivo con todas las aerolineas en
     * formato CSV, separa los campos para generar las aerolineas apropiadas
     *
     * @param linea un String con valores separados por coma
     * @return un objeto aerolinea
     */
    private Aerolinea procesaAerolineaDesdeStringCSV(String linea) {
        String nombre = linea;
        return new Aerolinea(nombre);
    }

    /**
     * Lee una lista csv con la informacion de todos los empleados y la usa para
     * obtener su informacion y crear los apropiadas objetos.
     *
     * @return un ArrayList con todos los objetos empleado.
     * @throws IOException si los archivos de texto son borrados o se les cambia
     * la dirreccion
     */
    private ArrayList<Empleado> cargarEmpleados() throws IOException {

        ArrayList<Empleado> empleados = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(RUTA_ARCHIVO_USUARIOS))) {
            // Salta primera linea porque no contiene info relevante
            br.readLine();

            String linea;
            while ((linea = br.readLine()) != null) {
                empleados.add(procesaEmpleadoDesdeStringCSV(linea));
            }
        }

        return empleados;
    }

    /**
     * De una linea de texto obtenida en un archivo con todos los empleados en
     * formato CSV, separa los campos para generar el empleado apropiado
     *
     * @param linea un String con valores separados por coma
     * @return un objeto empleado
     */
    private Empleado procesaEmpleadoDesdeStringCSV(String linea) {

        String cedula, nombre, apellido, email, usuario, clave, rol, aerolinea, departamento;
        Empleado empleado;

        ArrayList<String> valores = new ArrayList<String>(Arrays.asList(linea.split(",")));

        cedula = valores.get(0);
        nombre = valores.get(1);
        apellido = valores.get(2);
        email = valores.get(3);
        usuario = valores.get(4);
        clave = valores.get(5);
        rol = valores.get(6).toUpperCase();
        aerolinea = valores.get(7);
        departamento = valores.get(8);

        switch (rol) {
            case "A":
                empleado = new Administrador(cedula, email, departamento, usuario, clave, rol);
                break;
            case "C":
                // aqui hay un error. Encontrar la forma de obtener objetos Aerolinea apropiados
                empleado = new Cajero(cedula, email, departamento, usuario, clave, new Aerolinea(aerolinea), rol);
                break;
            case "P":
                empleado = new Planificador(cedula, email, departamento, usuario, clave, rol, new Aerolinea(aerolinea));
                break;
            default:
                empleado = null;
        }

        return empleado;

    }

    /**
     * Recorre la lista de todos los empleados y verifica que la combinacion de
     * clave y usuario tengan match en alguno de ellos.
     *
     * @param usuario
     * @param clave
     * @return true si se encontro un match, falso de lo contrario
     */
    private boolean validarLogin(String usuario, String clave) {

        for (Empleado empleado : empleados) {
            if (empleado.getUsuario().equals(usuario)) {
                if (empleado.getClave().equals(clave)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Metodo utilitario para debugear.
     */
    void imprimirEmpleadosAConsola() {
        for (Empleado empleado : empleados) {
            System.out.println(empleado);
        }
    }

    /**
     * Metodo utilitario para debugear.
     */
    void imprimirAerolineasAConsola() {
        for (Aerolinea aerolinea : aerolineas) {
            System.out.println(aerolinea);
        }
    }

    /**
     * Recorre todas las aerolineas ya cargadas y compara sus nombres con un
     * String para verificar su existencia.
     *
     * @param nombreDeAeroAVerificar es el posible nombre de una aerolinea
     * @return true si el nombre de esa aerolinea es igual al de un objeto
     * aerolinea ya cargardo
     */
    private boolean existeAerolineaPreviamente(String nombreDeAeroAVerificar) {
        for (Aerolinea aerolinea : aerolineas) {
            if (aerolinea.getNombre().equalsIgnoreCase(nombreDeAeroAVerificar)) {
                return true;
            }
        }

        return false;
    }
}
