/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema_aeropuerto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.TemporalQueries.localDate;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase que representa el objeto vuelo
 * @author acast
 */
public class Vuelo {

    String codigo_de_vuelo, codigo_IATA1_arribo, codigo_IATA1_salida;
    LocalDate fecha_de_embarque, fecha_de_salida, fecha_de_arribo;
    LocalTime hora_de_embarque, hora_de_salida, hora_de_arribo;
    String puerta_de_arribo, puerta_de_salida;
    String categorias;
    String cadena;
    String lugar_de_arribo;
    Scanner scanner = new Scanner(System.in);
    String prueba;
    HashMap<String, Integer> asientos_disponibles;

    public Vuelo(String codigo, String codigo_IATA1_arribo, String codigo_IATA1_salida, LocalDate fecha_de_embarque, LocalDate fecha_de_salida, LocalDate fecha_de_arribo, LocalTime hora_de_embarque, LocalTime hora_de_arribo, LocalTime hora_de_salida, String puerta_de_salida, String puerta_de_arribo, String negocios,String economico, String lugar_de_arribo) {
        this.codigo_de_vuelo = codigo;
        this.codigo_IATA1_arribo = codigo_IATA1_arribo;
        this.codigo_IATA1_salida = codigo_IATA1_salida;
        this.fecha_de_embarque = fecha_de_embarque;
        this.fecha_de_salida = fecha_de_salida;
        this.fecha_de_arribo = fecha_de_arribo;
        this.hora_de_embarque = hora_de_embarque;
        this.hora_de_salida = hora_de_salida;
        this.hora_de_arribo = hora_de_arribo;
        this.puerta_de_arribo = puerta_de_arribo;
        this.puerta_de_salida = puerta_de_salida;
        this.lugar_de_arribo = lugar_de_arribo;
        this.asientos_disponibles = new HashMap();
        this.asientos_disponibles.put("negocios", parseInt(negocios));
        this.asientos_disponibles.put("economico", parseInt(economico));
        
    }

    //metodo crear archvo vuelo
    public File crear_archivo_vuelos(String nombre) throws IOException {
        File vuelo = new File(nombre);
        if (!vuelo.exists()) {
            vuelo.createNewFile();

        }
        return vuelo;
    }



    /**
     * Verifica el cruze de horarios
     * @param nombre
     * @param hora
     * @param fecha
     * @return
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public boolean verificacion_cruze_horario_arribo(String nombre, LocalTime hora, LocalDate fecha) throws FileNotFoundException, IOException {
        String cadena;
        FileReader f = new FileReader(nombre);
        BufferedReader b = new BufferedReader(f);
        cadena = b.readLine();
        while (cadena != null) {

            System.out.println(cadena);
            String[] lista = cadena.split(",");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate fecha_salida1 = LocalDate.parse(lista[4], formatter);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("H:mm:ss");
            LocalTime hora_salida1 = LocalTime.parse(lista[8], dtf);
            System.out.println(fecha);
            System.out.println(hora_salida1);
            if ((fecha.compareTo(fecha_salida1)) == 0) {
                if (hora.isBefore(hora_salida1)) {
                    return true;
                }
            }
           cadena= b.readLine();
        }
        return false;
    }

    /**
     * Verifica que no se pase del limite de 8
     * @param nombre
     * @param num
     * @param hora
     * @return 
     */
    public boolean hay_8_puertas_ocupadas(String nombre, String num, LocalTime hora) {
        FileReader f = null;
        try {
            String cadena;
            f = new FileReader(nombre);
            BufferedReader b = new BufferedReader(f);
            try {
                int contador = 0;
                while ((cadena = b.readLine()) != null) {
                    //puerta de arribo
                    String[] lista = cadena.split(",");
                    String puerta_arribo = lista[10];

                    //hora
                    String hora_salida = lista[8];
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("H:mm:ss");
                    LocalTime hora_salida1 = LocalTime.parse(hora_salida, dtf);

                    
                    if(contador <= 8 & num == puerta_arribo & hora_salida1.isBefore(hora)) {

                        contador += 1;

                        if (contador == 8) {
                            return true;
                        }
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(Vuelo.class.getName()).log(Level.SEVERE, null, ex);
            }
            return false;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Vuelo.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                f.close();
            } catch (IOException ex) {
                Logger.getLogger(Vuelo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Vuelo{" + "codigo_de_vuelo=" + codigo_de_vuelo + ", codigo_IATA1_arribo=" + codigo_IATA1_arribo + ", codigo_IATA1_salida=" + codigo_IATA1_salida + ", fecha_de_embarque=" + fecha_de_embarque + ", fecha_de_salida=" + fecha_de_salida + ", fecha_de_arribo=" + fecha_de_arribo + ", hora_de_embarque=" + hora_de_embarque + ", hora_de_salida=" + hora_de_salida + ", hora_de_arribo=" + hora_de_arribo + ", puerta_de_arribo=" + puerta_de_arribo + ", puerta_de_salida=" + puerta_de_salida + ", categorias=" + categorias + ", cadena=" + cadena + ", lugar_de_arribo=" + lugar_de_arribo + ", scanner=" + scanner + ", prueba=" + prueba + ", asientos_disponibles=" + asientos_disponibles + '}';
    }



}
